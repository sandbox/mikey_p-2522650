<?php

/**
 * @file
 * Contains \Drupal\menu_link_content\Controller\MenuController.
 */

namespace Drupal\menu_link_fields\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\system\MenuInterface;

/**
 * Defines a route controller for a form for menu link content entity creation.
 *
 * This overrides the default controller to allow setting a bundle correctly.
 */
class MenuController extends ControllerBase {

  /**
   * Provides the menu link creation form.
   *
   * @param \Drupal\system\MenuInterface $menu
   *   An entity representing a custom menu.
   *
   * @return array
   *   Returns the menu link creation form.
   */
  public function addLink(MenuInterface $menu) {
    $menu_link = $this->entityManager()->getStorage('menu_link_content')->create(array(
      'id' => '',
      'parent' => '',
      'menu_name' => $menu->id(),
      'bundle' => $menu->id(), // This differs from the default controller, which doesn't allow a dynamic bundle.
    ));
    return $this->entityFormBuilder()->getForm($menu_link);
  }

}
