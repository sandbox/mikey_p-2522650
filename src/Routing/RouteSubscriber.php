<?php
/**
 * @file
 * Contains \Drupal\menu_link_fields\Routing\RouteSubscriber.
 */

namespace Drupal\menu_link_fields\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RoutingEvents::ALTER] = array('onAlterRoutes', -100);
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.menu.add_link_form')) {
      $route->setDefault('_controller', '\Drupal\menu_link_fields\Controller\MenuController::addLink');
    }

    // Disable display modes
    if ($route = $collection->get('entity.entity_view_display.menu_link_content.default')) {
      $route->setRequirement('_access', 'FALSE'); // Last parameter is always a string
    }
    if ($route = $collection->get('entity.entity_view_display.menu_link_content.view_mode')) {
      $route->setRequirement('_access', 'FALSE'); // Last parameter is always a string
    }
  }

}
